import 'package:flutter/material.dart';
import 'package:ug_mobile/screens/home_page.dart';
import 'package:ug_mobile/screens/news_page.dart';
import 'package:ug_mobile/screens/events_page.dart';
import 'package:ug_mobile/screens/alerts_page.dart';
import 'package:ug_mobile/screens/directory_page.dart';
import 'package:ug_mobile/screens/login_page.dart';

void main() => runApp(UGMobile());

class UGMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'University of Guyana',
      theme: ThemeData(
        primarySwatch: ugGreen,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(
              title: 'University of Guyana',
            ),
        '/news': (context) => NewsPage(),
        '/events': (context) => EventsPage(),
        '/alerts': (context) => AlertsPage(),
        '/directory': (context) => DirectoryPage(),
        '/login': (context) => LoginPage(),
      },
    );
  }
}

const MaterialColor ugGreen =
    const MaterialColor(0xFF217152, const <int, Color>{
  50: const Color.fromRGBO(21, 71, 52, 1),
  100: const Color.fromRGBO(21, 71, 52, 1),
  200: const Color.fromRGBO(21, 71, 52, 1),
  300: const Color.fromRGBO(21, 71, 52, 1),
  400: const Color.fromRGBO(21, 71, 52, 1),
  500: const Color.fromRGBO(21, 71, 52, 1),
  600: const Color.fromRGBO(21, 71, 52, 1),
  700: const Color.fromRGBO(21, 71, 52, 1),
  800: const Color.fromRGBO(21, 71, 52, 1),
  900: const Color.fromRGBO(21, 71, 52, 1),
});
