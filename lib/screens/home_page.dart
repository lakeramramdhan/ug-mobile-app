import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Lakeram Ramdhan"),
              accountEmail: Text("lakeramramdhan@uog.edu.gy"),
              currentAccountPicture: CircleAvatar(
                child: Text(
                  "L",
                  style: TextStyle(fontSize: 40.0),
                ),
              ),
            ),
            ListTile(
              title: Text("USI Profile"),
              onTap: () {
                Navigator.pushNamed(context, '/login');
              },
              leading: Icon(Icons.person),
            ),
            ListTile(
              title: Text("Academic Profile"),
              leading: Icon(Icons.assignment),
            ),
            Divider(),
            ListTile(
              title: Text("Alerts"),
              leading: Icon(Icons.rss_feed),
            ),
            ListTile(
              title: Text("Events"),
              leading: Icon(Icons.event),
            ),
            ListTile(
              onTap: () {
                Navigator.pushNamed(context, '/news');
              },
              title: Text("Newsletters"),
              leading: Icon(Icons.description),
            ),
            ListTile(
              title: Text("Campus Directory"),
              leading: Icon(Icons.book),
            ),
            Divider(),
            ListTile(
              title: Text("Settings"),
              leading: Icon(Icons.settings),
            ),
            ListTile(
              title: Text("Sign Out"),
              leading: Icon(Icons.exit_to_app),
            ),
            AboutListTile(
              icon: Icon(Icons.info),
              applicationIcon: Image.asset(
                'assets/images/app_logo-web.png',
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              ),
              applicationName: 'UG Mobile',
              applicationVersion: 'Version 1.0.0',
              applicationLegalese: 'Copyright@ 2019 University of Guyana',
            )
          ],
        ),
      ),
      body: GridView.count(
        crossAxisCount: 4,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          new GridTile(
            child: new Card(
              child: new Center(
                child: new ListView(
                  children: <Widget>[
                    Icon(Icons.rss_feed, size: 50),
                    Text(
                      'Alerts',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                ),
              ),
            ),
          ),
          new GridTile(
            child: new Card(
              child: new Center(
                child: new ListView(
                  children: <Widget>[
                    Icon(Icons.event, size: 50),
                    Text(
                      'Events',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                ),
              ),
            ),
          ),
          new GridTile(
            child: new Card(
              child: new Center(
                child: new ListView(
                  children: <Widget>[
                    Icon(Icons.description, size: 50),
                    Text(
                      'News',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                ),
              ),
            ),
          ),
          new GridTile(
            child: new Card(
              child: new Center(
                child: new ListView(
                  children: <Widget>[
                    Icon(Icons.book, size: 50),
                    Text(
                      'Directory',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
