import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ug_mobile/services/news_rss_service.dart';
import 'package:ug_mobile/services/webview_container.dart';

class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('UG News'),
      ),
      body: FutureBuilder(
        future: UGNewsRssService().getFeed(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              if (snapshot.hasData) {
                if (snapshot.data != null) {
                  final feed = snapshot.data;
                  return ListView.builder(
                      itemCount: feed.items.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        final item = feed.items[index];
                        var publishDate = item.pubDate;
                        return ListTile(
                          leading: Image.network(''),
                          title: Text(item.title),
                          subtitle: Text('Published at ' +
                              publishDate.toString().substring(0, 16)),
                          //DateFormat.d().format(DateTime.parse(item.pubDate))),
                          contentPadding: EdgeInsets.all(16.0),
                          onTap: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        WebViewContainer(item.link)));
                            //.replaceFirst('http', 'https'))));
                          },
                        );
                      });
                }
              }
              break;
            case ConnectionState.none:
            case ConnectionState.active:
            case ConnectionState.waiting:
            default:
              return Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              );
          }
        },
      ),
    );
  }
}
