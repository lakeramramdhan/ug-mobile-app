import 'package:http/http.dart' as http;
import 'package:webfeed/webfeed.dart';

class UGNewsRssService {
  final _targetUrl = 'https://uog.edu.gy/newsletters-feeds';

  Future<RssFeed> getFeed() =>
      http.read(_targetUrl).then((xmlString) => RssFeed.parse(xmlString));
}
