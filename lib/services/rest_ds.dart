import 'dart:async';
import 'dart:convert';
import 'network_util.dart';
import 'package:crypto/crypto.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL =
      "http://52.23.183.60/srms/student/current/registration";
  static final LOGIN_URL = BASE_URL + "/rest_info.php";
  static String action = 'login';

  Future login(String usi, String password) {
    String _md5usi = md5.convert(utf8.encode(usi)).toString();
    return _netUtil.post(LOGIN_URL, body: {
      "action": action,
      "usi": usi,
      "pin": password,
      "code": _md5usi
    }).then((dynamic res) {
      print(res.toString());
      if (res["error"]) throw new Exception(res["error_msg"]);
    });
  }
}
